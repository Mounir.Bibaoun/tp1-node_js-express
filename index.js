const express = require('express')
const app = express()
const equipes = require('./equipes.json')
const joueurs = require('./joueurs.json')

//const body_parser = require('body-parser')
app.use(express.json())

// equipes :::


app.get('/equipes', (req, res) => {
    // res.send("Liste des equipes")
    res.status(200).json(equipes)
});
app.get('/equipes/:id', (req, res) => {
    const id = parseInt(req.params.id)
    const equipe = equipes.find(equipe => equipe.id === id)
    res.status(200).json(equipe)
});
app.post('/equipes', (req, res) => {
    equipes.push(req.body)
    res.status(200).json(equipes)
});
app.put('/equipes/:id', (req, res) => {
    const id = parseInt(req.params.id)
    let equipe = equipes.find(equipe => equipe.id === id)
    equipe.name = req.body.name,
    equipe.country = req.body.country,
    res.status(200).json(equipe)
});


// ##################################################################################
//Exercise 1 (joueurs) :
//Q1
app.get('/joueurs', (req, res) => {
    res.status(200).json(joueurs);
});
app.get('/joueurs/:id', (req, res) => {
    const id = parseInt(req.params.id)
    const joueur = joueurs.find(joueur => joueur.id === id)
    res.status(200).json(joueur)
});
app.post('/joueurs', (req, res) => {
    joueurs.push(req.body)
    res.status(200).json(joueurs)
});
app.put('/joueurs/:id', (req, res) => {
    const id = parseInt(req.params.id)
    let joueur = joueurs.find(joueur => joueur.id === id)
    joueur.idEquipe = req.body.idEquipe,
    joueur.nom = req.body.nom,
    joueur.numero = req.body.numero,
    joueur.poste = req.body.poste,
    res.status(200).json(joueur)
});

// Q2  
app.get('/equipes/:id/joueurs', (req, res) => {
    const equipeId = parseInt(req.params.id);
const equipe = equipes.find(equipe => equipe.id === equipeId)
    const joueursDeLEquipe = joueurs.filter(joueur => joueur.idEquipe === equipe.id);
    if (joueursDeLEquipe.length === 0) {
        res.status(404).send('Aucun joueur trouvé pour cette équipe.');
        return;
    }
    res.status(200).json(joueursDeLEquipe);
}); 

// Q3 
app.get('/joueurs/:id/equipes', (req, res) => {
    const playerId = parseInt(req.params.id);
    const joueur = joueurs.find(joueur => joueur.id === playerId);

    if (!joueur) {
        res.status(404).send('Joueur non trouvé.');
        return;
    }

    const equipeId = joueur.idEquipe;
    const equipe = equipes.find(equipe => equipe.id === equipeId);

    if (!equipe) {
        res.status(404).send('Aucune équipe correspondante trouvée pour ce joueur.');
        return;
    }

    const joueursDeLEquipe = joueurs.filter(joueur => joueur.idEquipe === equipeId);
    res.status(200).json(equipe);
});


// Q4

app.get('/joueurs/search', (req, res) => {
    const searchTerm = req.query.nom.toLowerCase();
    const joueur = joueurs.find(joueur => joueur.nom.toLowerCase() === searchTerm);

    if (joueur) {
        res.status(200).json(joueur);
    } else {
        res.status(404).send("Joueur non trouvé");
    }
});




